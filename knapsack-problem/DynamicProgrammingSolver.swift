//
// Created by Arkadiusz Żmudzin on 2019-05-31.
// Copyright (c) 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

class DynamicProgrammingKnapsackSolver: KnapsackProblemSolver {
    var maxWeight: Int
    var items: [Item]
    private var dpArray: [[Int]] = []

    required init(maxWeight: Int, items: [Item]) {
        self.maxWeight = maxWeight
        self.items = items
    }

    func solve() -> Int {
        initArray()
        fillUpTable()

        return answer()
    }

    private func initArray() {
        for _ in 0...items.count {
            dpArray.append(Array<Int>(repeating: 0, count: maxWeight + 1))
        }
    }

    private func fillUpTable() {
        for i in stride(from: items.count - 1, through: 0, by: -1) {
            for j in 0...maxWeight {
                let item: Item = items[i]

                let notTaken = dpArray[i + 1][j]
                let taken = (j - item.weight >= 0) ? items[i].value + dpArray[i + 1][j - item.weight] : 0;

                dpArray[i][j] = max(notTaken, taken)
            }
        }
    }

    private func answer() -> Int {
        return dpArray[0][maxWeight]
    }
}
