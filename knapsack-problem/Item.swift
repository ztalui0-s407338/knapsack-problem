//
// Created by Arkadiusz Żmudzin on 2019-05-31.
// Copyright (c) 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

struct Item: CustomStringConvertible {
    let value: Int
    let weight: Int

    var description: String {
        return "[value: \(value), weight: \(weight)]"
    }
}
