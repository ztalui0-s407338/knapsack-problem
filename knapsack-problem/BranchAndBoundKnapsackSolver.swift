//
// Created by Arkadiusz Żmudzin on 2019-06-01.
// Copyright (c) 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

class BranchAndBoundKnapsackSolver: KnapsackProblemSolver {
    var maxWeight: Int
    var items: [Item]

    required init(maxWeight: Int, items: [Item]) {
        self.maxWeight = maxWeight
        self.items = items
    }

    func solve() -> Int {
        let sortedItems = self.items.sorted { item, item2 in
            // sort by value/weight ratio descending
            let ratio1 = Double(item.value) / Double(item.weight)
            let ratio2 = Double(item2.value) / Double(item2.weight)

            return ratio1 > ratio2
        }

        var queue = Queue<Node>()
        var parentNode = Node(level: -1, profit: 0, bound: 0, weight: 0)
        var tempNode = Node(level: -1, profit: 0, bound: 0, weight: 0)
        var maxProfit = 0

        queue.enqueue(parentNode)

        while !queue.isEmpty {
            parentNode = queue.dequeue()!

            if parentNode.level == -1 {
                tempNode.level = 0
            }

            if parentNode.level == sortedItems.count - 1 {
                continue
            }

            tempNode.level = parentNode.level + 1

            tempNode.weight = parentNode.weight + sortedItems[tempNode.level].weight
            tempNode.profit = parentNode.profit + sortedItems[tempNode.level].value

            if tempNode.weight <= maxWeight && tempNode.profit > maxProfit {
                maxProfit = tempNode.profit
            }

            tempNode.bound = tempNode.profitBound(maxWeight: maxWeight, items: sortedItems)
            if tempNode.bound > maxProfit {
                queue.enqueue(tempNode)
            }
        }

        return maxProfit
    }

    private struct Node {
        var level: Int
        var profit: Int
        var bound: Int
        var weight: Int

        func profitBound(maxWeight: Int, items: [Item]) -> Int {
            if self.weight >= maxWeight {
                return 0
            }

            var profitBound = self.profit

            var j = self.level + 1
            var totalWeight = self.weight

            while (j < items.count) && (totalWeight + items[j].weight <= maxWeight) {
                totalWeight += items[j].weight
                profitBound += items[j].value
                j += 1
            }

            return profitBound
        }
    }
}
