//
// Created by Arkadiusz Żmudzin on 2019-05-31.
// Copyright (c) 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

class GreedyKnapsackSolver: KnapsackProblemSolver {
    var maxWeight: Int
    var items: [Item]

    required init(maxWeight: Int, items: [Item]) {
        self.maxWeight = maxWeight
        self.items = items
    }

    func solve() -> Int {
        let sortedItems = self.items.sorted { item, item2 in
            // sort by value/weight ratio descending
            let ratio1 = Double(item.value) / Double(item.weight)
            let ratio2 = Double(item2.value) / Double(item2.weight)

            return ratio1 > ratio2
        }

        var i = 0;
        var weight = 0;
        var solutionItems: [Item] = []

        while (i < items.count && weight <= maxWeight) {
            let consideredItem = sortedItems[i]
            if weight + consideredItem.weight <= maxWeight {
                solutionItems.append(consideredItem)
                weight += consideredItem.weight
            }
            i += 1;
        }

        return solutionItems.map({ item in item.value }).reduce(0) { acc, val in acc + val }
    }

}
