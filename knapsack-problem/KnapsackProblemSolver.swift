//
// Created by Arkadiusz Żmudzin on 2019-05-31.
// Copyright (c) 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

protocol KnapsackProblemSolver {
    var maxWeight: Int { get }
    var items: [Item] { get }

    init(maxWeight: Int, items: [Item])

    func solve() -> Int
}
