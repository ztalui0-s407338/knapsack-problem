//
//  main.swift
//  knapsack-problem
//
//  Created by Arkadiusz Żmudzin on 2019-05-31.
//  Copyright © 2019 Arkadiusz Żmudzin. All rights reserved.
//

import Foundation

let maxWeight: Int = 400
let itemsCount: Int = 1000

let items: [Item] = Array<Int>(repeating: 0, count: itemsCount).map({ _ in
    return Item(value: Int.random(in: 0...maxWeight), weight: Int.random(in: 0...maxWeight))
})

let dpTime = measureExecution(withTag: "dynamic programming") { tag in
    let startMemory = usedMemory() ?? 0
    let dpSolver = DynamicProgrammingKnapsackSolver(maxWeight: maxWeight, items: items)
    let result: Int = dpSolver.solve()
    let endMemory = usedMemory() ?? 0
    print("\(tag): used memory: \(endMemory - startMemory) bytes")
    return result
}

let greedyTime = measureExecution(withTag: "greedy") { tag in
    let startMemory = usedMemory() ?? 0
    let greedySolver = GreedyKnapsackSolver(maxWeight: maxWeight, items: items)
    let result: Int = greedySolver.solve()
    let endMemory = usedMemory() ?? 0
    print("\(tag): used memory: \(endMemory - startMemory) bytes")
    return result
}

let branchAndBoundTime = measureExecution(withTag: "branch and bound") { tag in
    let startMemory = usedMemory() ?? 0
    let bbSolver = BranchAndBoundKnapsackSolver(maxWeight: maxWeight, items: items)
    let result: Int = bbSolver.solve()
    let endMemory = usedMemory() ?? 0
    print("\(tag): used memory: \(endMemory - startMemory) bytes")
    return result
}

